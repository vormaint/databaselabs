﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace LibDB5
{
    public class Tasks
    {
        //Строка соединения.
        public static string ConnectionString = "Data Source=DESKTOP-1T99363;Initial Catalog=FootballClub;Integrated Security=True";

        /// <summary>
        /// Метод, который выводит данные о работниках и их должности.
        /// </summary>
        public static void EmployeePosition()
        {
            Console.Clear();

            string sqlExpression = "SELECT Employees.LastName, Employees.Name, Employees.MiddleName, Positions.PositionName FROM Employees " +
                "INNER JOIN Contracts ON Employees.ContractID = Contracts.ContractID " +
                "INNER JOIN Positions ON Contracts.PositionID = Positions.PositionID";
            int Number = 1;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        string LastName = reader.GetString(0);
                        string Name = reader.GetString(1);
                        string MiddleName = reader.GetString(2);
                        string PositionName = reader.GetString(3);

                        Console.WriteLine("--------------------");
                        Console.WriteLine(Number.ToString() +".\n"+ "LastName:" + LastName);
                        Console.WriteLine("Name:" + Name);
                        Console.WriteLine("MiddleName:" + MiddleName);
                        Console.WriteLine("PositionName:" + PositionName);
                        Number++;
                    }
                }
                reader.Close();
                Console.WriteLine("\nНажмите любую кнопку для продолжения...");
                Console.ReadKey();
            }

        }

        /// <summary>
        /// Метод, который подсчитывает количество сыграных матчей
        /// </summary>
        public static void AmountOfMatches()
        {
            Console.Clear();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlCommand command = new SqlCommand("Select SUM(AmountOfMatches) * 22/(COUNT(*)) FROM Player;", connection);
                int RecordsExist = (int)command.ExecuteScalar();
                Console.WriteLine("Количество сыграных матчей: " + RecordsExist.ToString());
                Console.WriteLine("\nНажмите любую кнопку для продолжения...");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// выводит информацию о игроках с зарплатой больше 5000
        /// </summary>
        public static void SalaryMore5000()
        {
            Console.Clear();

            string sqlExpression = "SELECT Employees.LastName, Employees.Name, Employees.MiddleName FROM Employees " +
                "INNER JOIN Contracts ON Employees.ContractID = Contracts.ContractID " +
                "INNER JOIN Positions ON Contracts.PositionID = Positions.PositionID " +
                "WHERE Salary > 5000";
            int Number = 1;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        string LastName = reader.GetString(0);
                        string Name = reader.GetString(1);
                        string MiddleName = reader.GetString(2);

                        Console.WriteLine("--------------------");
                        Console.WriteLine(Number.ToString() + ".\n" + "LastName:" + LastName);
                        Console.WriteLine("Name:" + Name);
                        Console.WriteLine("MiddleName:" + MiddleName);
                        Number++;
                    }
                }
                reader.Close();
                Console.WriteLine("\nНажмите любую кнопку для продолжения...");
                Console.ReadKey();

            }
        }

        /// <summary>
        /// Метод, который получает с БД уникальные значения позиций игроков
        /// </summary>
        /// <returns>Список позиций</returns>
        public static List<string> DistinctPlayerPosition()
        {
            string sqlExpression = "SELECT DISTINCT Position FROM Player";
            int Number = 1;
            Console.WriteLine("Позиции:");
            List<string> Positions = new List<string>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        string Position = reader.GetString(0);
                        Positions.Add(Position);
                        Console.WriteLine(Number.ToString() + ". " + Position);
                        Number++;
                    }
                }
                reader.Close();
                return Positions;
            }
        }

        /// <summary>
        /// Метод, который реализует при помощи параметров. Выводи информацию о игроках на определенной позиции.
        /// </summary>
        /// <param name="Position">Позиция</param>
        public static void PlayersOnPosition(string Position)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();
                SqlParameter nameParam = new SqlParameter("@Position", Position.ToString());
                SqlCommand command = new SqlCommand("SELECT Employees.LastName, Employees.Name, Employees.MiddleName " +
                    "FROM Player INNER JOIN Employees ON Player.EmployeeID = Employees.EmployeeID " +
                    "WHERE Position = @Position;", connection);
                command.Parameters.Add(nameParam);
                int Number = 1;

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        string LastName = reader.GetString(0);
                        string Name = reader.GetString(1);
                        string MiddleName = reader.GetString(2);

                        Console.WriteLine("--------------------");
                        Console.WriteLine(Number.ToString() + ".\n" + "LastName:" + LastName);
                        Console.WriteLine("Name:" + Name);
                        Console.WriteLine("MiddleName:" + MiddleName);
                        Number++;
                    }
                }
                Console.WriteLine("\nНажмите любую кнопку для продолжения...");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Подсчитывает средний возраст игроков на позиции
        /// </summary>
        /// <param name="Position">Позиция</param>
        public static void AvgPosAge(string Position)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();
                SqlParameter nameParam = new SqlParameter("@Position", Position.ToString());
                SqlCommand command = new SqlCommand("SELECT AVG(YEAR(GETDATE()) - YEAR(Employees.Birthday)) " +
                    "FROM  Employees INNER JOIN Player ON Employees.EmployeeID = Player.EmployeeID " +
                    "WHERE  (Player.Position = @Position)", connection);
                command.Parameters.Add(nameParam);
                int RecordsExist = (int)command.ExecuteScalar();
                Console.WriteLine("\nСредний возраст на позиции " + Position+ ": " + RecordsExist.ToString());
                Console.WriteLine("\nНажмите любую кнопку для продолжения...");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Метод, для добавления нового игрока
        /// </summary>
        /// <param name="Goals">Количество голов</param>
        /// <param name="Passes">Количество пасов</param>
        /// <param name="AmountOfMatch">Количество сыграных матчей</param>
        /// <param name="Player_Position">Позиция игрока</param>
        /// <param name="PlayerID">ИД игрока</param>
        public static void AddNewPlayer(int Goals, int Passes, int AmountOfMatch, string Player_Position, int PlayerID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlCommand command = new SqlCommand();
                string strSql = "INSERT INTO Player (Goals, Passes, AmountOfMatches, Position, EmployeeID) VALUES";
                strSql += "('" + Goals.ToString() + "', '" + Passes.ToString() + "', '"
                    + AmountOfMatch.ToString() + "', '" + Player_Position + "', '" + PlayerID.ToString() + "')";

                command = new SqlCommand(strSql, connection);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Метод, для удаления игрока.
        /// </summary>
        /// <param name="PlayerID">Ид игрока</param>
        public static void DeletePlayer(int PlayerID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlCommand command = new SqlCommand();
                string strSql = "DELETE FROM Player WHERE PlayerID = " + PlayerID.ToString();

                command = new SqlCommand(strSql, connection);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Метод для перекрестного запроса
        /// </summary>
        /// <param name="Parametrs">Параметры</param>
        /// <param name="Position">Список позиций</param>
        public static void CrossRequest(string Parametrs, List<string> Position)
        {
            Console.Clear();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT [Amount_]," + Parametrs + 
                    "FROM (SELECT 'Amount Matches' AS 'Amount_', Position, AmountOfMatches FROM Player) x " +
                    "PIVOT (SUM(AmountOfMatches) FOR Position IN(" + Parametrs + ")) pvt;", connection);

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        for (int i = 1; i <= Position.Count; i++)
                        {
                            Console.Write("Количество матчей на позиции " + Position[i - 1] + " : ");
                            Console.WriteLine(reader.GetInt32(i));
                        }
                    }
                }
                Console.WriteLine("\nНажмите любую кнопку для продолжения...");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Метод, для обновления данных
        /// </summary>
        /// <param name="NewValue">Новое значение</param>
        /// <param name="PlayerID">ИД игрока</param>
        public static void UpdatePlayer(int NewValue, int PlayerID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlCommand command = new SqlCommand();
                string strSql = "UPDATE Player Set Goals = "+ NewValue.ToString() +" WHERE PlayerID = " + PlayerID.ToString();

                command = new SqlCommand(strSql, connection);
                command.ExecuteNonQuery();
            }
        }
    }
}