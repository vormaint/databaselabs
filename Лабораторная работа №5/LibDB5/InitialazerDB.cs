﻿using System;
using System.Data.SqlClient;
namespace LibDB5
{
//-- 1) Сотрудники
//-- 2) Должности
//-- 3) Страны
//-- 4) Команды
//-- 5) Достижения
//-- 6) Контракты

    public class InitialazerDB
    {
        public static string Initialize(string connectionString)
        {
            //Инициализация данных для заполнения.
            //Для простоты для каждой таблицы для каждого поля создается своя переменная во избежание путаницы.

            //Для стран.
            int Countries_number = 100;
            string Name;
            string Discribe;

            //Для должностей.
            int Positions_number = 100;
            string PositionName;
            int Salary;
            string Responsibility;
            string Requirements;

            //Для контрактов.
            int Contracts_number = 10000;
            DateTime DateOfSigning;
            DateTime ExpirationDate;
            int PositionID;

            //Для сотрудников.
            int Employees_number = 10000;
            string LastName;
            string FirstName;
            string MiddleName;
            DateTime Birthday;
            int PhoneNumber;
            int ContractID;
            int EmpCountryID;

            //Для команд.
            int Teams_number = 10000;
            DateTime DateOfEstablishment;
            int CoutryID;
            int CouchID;

            //Для игроков.
            int Player_number = 10000;
            int Goals;
            int Passes;
            int AmountOfMatch;
            string Player_Position;

            string result = "";
            Random rnd = new Random();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                {
                    // Открытие соединения
                    connection.Open();

                    SqlCommand command;
                    try
                    {
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //Заполнение таблицы Страны.
                        //Словари для Стран.
                        string[] CountriesName = { "Беларусь", "Россия", "Украина", "Польша", "Литва", "Латвия", "Эстония", "Германия", "Франция", "Лихтеншейн" };//словарь названий стран
                        string[] CountriesDiscription = { "Стабильная", "Надежная", "Красивая", "Наполненная", "Самостоятельная", "Богатая", "Красочные пейзажи" };//словарь описаний

                        int count_CountriesName = CountriesName.Length;
                        int count_CountriesDiscription = CountriesDiscription.Length;

                        string strSql = "";

                        for (int CountryID = 1; CountryID <= Countries_number; CountryID++)
                        {
                            strSql += "INSERT INTO Countries (CountryName, CountryDescription) VALUES ";
                            Name = CountriesName[InitialazerDB.rnd.Next(0, count_CountriesName)];
                            Discribe = CountriesDiscription[InitialazerDB.rnd.Next(0, count_CountriesDiscription)];
                            strSql += "('" + Name.ToString() + "', '" + Discribe.ToString() + "')";
                        }
                        command = new SqlCommand(strSql, connection);
                        ////отправляет команду на вставку в базу данных
                        command.ExecuteNonQuery();
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        //Заполнение таблицы должностей.
                        //Словари для должностей.
                        string[] Position_Name = { "Менеджер", "Оператор", "Трененeр", "Игрок", "Швейцар", "Деректор клуба" };//словарь названий видов должностей
                        string[] Position_Responsibility = { "Играть в футбол", "Обслуживать клиентов",
                            "Тренировать команду", "Отвечать на заявки", "Руководить центром", "Управлять персоналом" };//словарь названий видов обязанностей
                        string[] Position_Requirements = { "Без вредных привычек", "Крепкое здоровье", "Стрессоустойчивость",
                            "Креативность", "Понимание текущих реалий", "Легкое нахождение общего языка" };//словарь названий видов требований

                        int count_Position_Name = Position_Name.Length;
                        int count_Position_Responsibility = Position_Responsibility.Length;
                        int count_Position_Requirements = Position_Requirements.Length;

                        strSql = "";
                        for (int Position_ID = 1; Position_ID <= Positions_number; Position_ID++)
                        {
                            strSql += "INSERT INTO Positions (PositionName, Salary, Responsibility, Requirements) VALUES";
                            PositionName = Position_Name[InitialazerDB.rnd.Next(0, count_Position_Name)];
                            Salary = InitialazerDB.rnd.Next(1000, 6000);
                            Responsibility = Position_Responsibility[InitialazerDB.rnd.Next(0, count_Position_Responsibility)];
                            Requirements = Position_Requirements[InitialazerDB.rnd.Next(0, count_Position_Requirements)] ;
                            strSql += "('" + PositionName.ToString() + "', '" + Salary.ToString() + "', '" 
                                + Responsibility.ToString() + "', '" + Requirements.ToString() + "')";
                        }
                        command = new SqlCommand(strSql, connection);
                        ////отправляет команду на вставку в базу данных
                        command.ExecuteNonQuery();
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        //Заполнение таблицы контракты.
                        strSql = "";
                        SqlCommand check_NumberPosition = new SqlCommand("SELECT MAX(PositionID) FROM Positions;", connection);
                        int RecordsExist = (int)check_NumberPosition.ExecuteScalar();

                        for (int i = 1; i <= Contracts_number; i++)
                        {
                            strSql += "INSERT INTO Contracts (DateOfSigning, ExpirationDate, PositionID) VALUES";
                            DateOfSigning = InitialazerDB.RandomDay();
                            ExpirationDate = InitialazerDB.RandomDay();
                            while (ExpirationDate < DateOfSigning)
                            {
                                ExpirationDate = InitialazerDB.RandomDay();
                            }
                            PositionID = InitialazerDB.rnd.Next(RecordsExist - Positions_number, RecordsExist + 1); ;

                            strSql += "('" + DateOfSigning.ToString("d") + "', '" + ExpirationDate.ToString("d") + "', '" + PositionID.ToString() + "')";
                        }
                        command = new SqlCommand(strSql, connection);
                        ////отправляет команду на вставку в базу данных
                        command.ExecuteNonQuery();
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        //Заполнение таблицы сотрудники.
                        string[] Employees_LastName = { "Скляренко", "Аксёнов", "Князев", "Токар", "Несвитайло", "Андрейко", "Фролов", "Елисеев", "Поляков", "Прохоров" };
                        string[] Employees_Name = { "Шамиль", "Милан", "Прохор", "Зигмунд", "Карен", "Георгий", "Ярослав", "Чарльз", "Виль", "Леонард" };
                        string[] Employees_MiddleName = { "Артёмович", "Петрович", "Валериевич", "Данилович", "Максимович", "Андреевич", "Викторович", "Анатолиевич", "Сергеевич", "Львович" };
                        strSql = "";

                        SqlCommand check_NumberContract = new SqlCommand("SELECT MAX(ContractID) FROM Contracts;", connection);
                        int NumberContractExist = (int)check_NumberContract.ExecuteScalar();

                        SqlCommand check_Countries = new SqlCommand("SELECT MAX(CountryID) FROM Countries;", connection);
                        int check_CountriesExist = (int)check_Countries.ExecuteScalar();

                        int Count_DifferentName = Employees_LastName.Length;

                        for (int Employee_ID = 1; Employee_ID <= Employees_number; Employee_ID++)
                        {
                            strSql += "INSERT INTO Employees (LastName, Name, MiddleName, Birthday, PhoneNumber, CountryID, ContractID) VALUES";
                            LastName = Employees_LastName[InitialazerDB.rnd.Next(0, Count_DifferentName)];
                            FirstName = Employees_Name[InitialazerDB.rnd.Next(0, Count_DifferentName)] ;
                            MiddleName = Employees_MiddleName[InitialazerDB.rnd.Next(0, Count_DifferentName)];
                            Birthday = InitialazerDB.RandomDay();
                            PhoneNumber = InitialazerDB.rnd.Next(1000000, 10000000);
                            ContractID = InitialazerDB.rnd.Next(NumberContractExist - Contracts_number, NumberContractExist + 1);
                            EmpCountryID = InitialazerDB.rnd.Next(check_CountriesExist - Countries_number, check_CountriesExist + 1);

                            strSql += "('" + LastName.ToString() + "', '" + FirstName.ToString() + "', '" + MiddleName.ToString() + "', '" + Birthday.ToString("d") + "', '"
                                + PhoneNumber.ToString() + "', '" + EmpCountryID.ToString() + "', '" + ContractID.ToString() + "')";
                        }
                        command = new SqlCommand(strSql, connection);
                        ////отправляет команду на вставку в базу данных
                        command.ExecuteNonQuery();
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        //Заполнение таблицы Teams.
                        strSql = "";

                        SqlCommand check_Employees = new SqlCommand("SELECT MAX(EmployeeID) FROM Employees;", connection);
                        int NumberEmployees = (int)check_Employees.ExecuteScalar();

                        check_Countries = new SqlCommand("SELECT MAX(CountryID) FROM Countries;", connection);
                        check_CountriesExist = (int)check_Countries.ExecuteScalar();

                        for (int Team_ID = 1; Team_ID <= Teams_number; Team_ID++)
                        {
                            strSql += "INSERT INTO Teams (CountryID, DateOfEstablishment, TeamID) VALUES";
                            DateOfEstablishment = InitialazerDB.RandomDay();
                            CoutryID = InitialazerDB.rnd.Next(check_CountriesExist - Countries_number, check_CountriesExist + 1);
                            CouchID = InitialazerDB.rnd.Next(NumberEmployees - Employees_number, NumberEmployees + 1);

                            strSql += "('" + CoutryID.ToString() + "', '" + DateOfEstablishment.ToString("d") + "', '" + CouchID.ToString() + "')";
                        }
                        command = new SqlCommand(strSql, connection);
                        ////отправляет команду на вставку в базу данных
                        command.ExecuteNonQuery();
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        //Заполнение таблицы игроки.
                        strSql = "";

                        string[] Dictionary_Player_Position = { "Нападающий", "Защитник", "Полузащитник", "Вратарь"};
                        int Count_Player_Position = Dictionary_Player_Position.Length;

                        SqlCommand check_Teams = new SqlCommand("SELECT MAX(TeamID) FROM Teams;", connection);
                        int NumberTeams = (int)check_Teams.ExecuteScalar();

                        for (int Achievement_ID = 1; Achievement_ID <= Player_number; Achievement_ID++)
                        {
                            strSql += "INSERT INTO Player (Goals, Passes, AmountOfMatches, Position, EmployeeID) VALUES";
                            Goals = InitialazerDB.rnd.Next(0, 100);
                            Passes = InitialazerDB.rnd.Next(0, 100);
                            AmountOfMatch = InitialazerDB.rnd.Next(0, 100);
                            Player_Position = Dictionary_Player_Position[InitialazerDB.rnd.Next(0, Count_Player_Position)];
                            int TeamID = InitialazerDB.rnd.Next(NumberTeams - Teams_number, NumberTeams + 1);

                            strSql += "('" + Goals.ToString() + "', '" + Passes.ToString() + "', '"
                                + AmountOfMatch.ToString() + "', '" + Player_Position + "', '" + TeamID.ToString() + "')";
                        }
                        command = new SqlCommand(strSql, connection);
                        ////отправляет команду на вставку в базу данных
                        command.ExecuteNonQuery();
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    }
                    // Обработка ошибок внутри транзакции
                    catch (Exception ex)
                    {
                        result = ex.Message;
                    }

                }
            }

            return result;
        }

        private static Random rnd = new Random();
        public static DateTime RandomDay()
        {
            DateTime start = new DateTime(1995, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(InitialazerDB.rnd.Next(range));
        }
    }
}
