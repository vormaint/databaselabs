﻿using System;
using System.Collections.Generic;
using LibDB5;

namespace ConsoleLab5
{
    class Program
    {
        static void Main(string[] args)
        {
            int Choice = new int();
            string connectionString = "Data Source=DESKTOP-1T99363;Initial Catalog=FootballClub;Integrated Security=True";

            while (Choice != 11)
            {
                Console.Clear();
                Console.WriteLine("1. Заполнить БД случайными значениями. (Занимает время)");
                Console.WriteLine("2. Вывести данные о рабочих и их должности.");
                Console.WriteLine("3. Количество сыграных матчей.");
                Console.WriteLine("4. Список игроков, сумма в контрактах которых превышает 5000 рублей.");
                Console.WriteLine("5. Параметрический запрос для отображения информации о спортсменах, играющих на определенных позициях.");
                Console.WriteLine("6. Перекрестный запрос с информацией о количестве матчей для всех игроков по каждой позиции.");
                Console.WriteLine("7. Вычислить средний возраст игроков, играющих на заданной позиции.");
                Console.WriteLine("8. Добавление нового футболиста.");
                Console.WriteLine("9. Обновление данных о футболисте.");
                Console.WriteLine("10. Удаление данных о футболисте.");
                Console.WriteLine("11. Выход.");
                Console.Write("\nВаш выбор:");
                Choice = int.Parse(Console.ReadLine());

                switch(Choice)
                {
                    case 1:
                        //Обращение к методу, который заполняет БД случайными значениями
                        InitialazerDB.Initialize(connectionString);
                        break;
                    case 2:
                        //Обращение к методу, который выводит данные и должность, каждого работника.
                        Tasks.EmployeePosition();
                        break;
                    case 3:
                        //Обращение к методу, который выводит количество сыгранных матчей.
                        //С учетом того что в одной игре участвовало 22 человека.
                        Tasks.AmountOfMatches();
                        break;
                    case 4:
                        //Выводит данные о игроках с заплатой больше 5000
                        Tasks.SalaryMore5000();
                        break;
                    case 5:
                        //Переменная для записи выбора.
                        int PositionChoise = 0;
                        //Получение списка УНИКАЛЬНЫЙ позиций игрков.
                        List<string> Positions = Tasks.DistinctPlayerPosition();
                        //Получение номера той позиции которую хочет пользователь
                        Console.Write("Введите номер позиции : ");
                        PositionChoise = int.Parse(Console.ReadLine());

                        //Перезапрос значения у пользователя, если выбрано значени вне массива.
                        while(PositionChoise > Positions.Count || PositionChoise < 1)
                        {
                            Console.Write("Введите номер позиции верно: ");
                            PositionChoise = int.Parse(Console.ReadLine());
                        }

                        //Обращение к методу, который выводит данные о всех игроках на текущей позиции.
                        Tasks.PlayersOnPosition(Positions[PositionChoise - 1]);
                        break;
                    case 6:
                        //Получение возможных значений 
                        Positions = Tasks.DistinctPlayerPosition();
                        string Paramers = "";
                        //Состовление строки для перекрестного запроса
                        for (int i = 0; i < Positions.Count; i++)
                        {
                            Paramers += "[" + Positions[i] + "]";
                            if(i != Positions.Count - 1)
                            {
                                Paramers += ", ";
                            }
                        }

                        //Обращению к методу, который возвращает количество ссыгранных матчей
                        //игроков играющих на разнах позициях.
                        Tasks.CrossRequest(Paramers, Positions);
                        break;
                    case 7:
                        PositionChoise = 0;
                        Positions = Tasks.DistinctPlayerPosition();
                        Console.Write("Введите номер позиции : ");
                        PositionChoise = int.Parse(Console.ReadLine());
                        while (PositionChoise > Positions.Count || PositionChoise < 1)
                        {
                            Console.Write("Введите номер позиции верно: ");
                            PositionChoise = int.Parse(Console.ReadLine());
                        }

                        //Обращению к методу, который вычисляет средний возраст игроков на текущей позиции.
                        Tasks.AvgPosAge(Positions[PositionChoise - 1]);
                        break;
                    case 8:
                        //Добавление нового игрока
                        Console.WriteLine("Добавление нового игрока : ");
                        Console.Write("Введите количество голов, которые он забил :");
                        int Goals = int.Parse(Console.ReadLine());

                        Console.Write("Введите количество пасов, которые он отдал :");
                        int Passes = int.Parse(Console.ReadLine());

                        Console.Write("Введите количество матчей, которые он сыграл : ");
                        int AmountOfMatches = int.Parse(Console.ReadLine());

                        PositionChoise = 0;
                        Positions = Tasks.DistinctPlayerPosition();
                        Console.Write("Введите номер позиции на которой он играет: ");
                        PositionChoise = int.Parse(Console.ReadLine());
                        while (PositionChoise > Positions.Count || PositionChoise < 1)
                        {
                            Console.Write("Введите номер позиции верно: ");
                            PositionChoise = int.Parse(Console.ReadLine());
                        }
                        Console.WriteLine("Введите ID сотрудника : ");
                        int ID = int.Parse(Console.ReadLine());

                        Tasks.AddNewPlayer(Goals, Passes, AmountOfMatches, Positions[PositionChoise - 1], ID);
                        break;
                    case 9:
                        //обновление игрока по ID
                        Console.WriteLine("Обновление игрока : ");
                        Console.Write("Введите ID игрока:");
                        ID = int.Parse(Console.ReadLine());
                        Console.Write("Введите количество голов у этого игрока : ");
                        Goals = int.Parse(Console.ReadLine());

                        Tasks.UpdatePlayer(Goals, ID);
                        break;
                    case 10:
                        //Удаление игрока по ID
                        Console.WriteLine("Удаление игрока : ");
                        Console.Write("Введите ID игрока:");
                        ID = int.Parse(Console.ReadLine());

                        Tasks.DeletePlayer(ID);
                        break;
                }
                
            }

        }
    }
}
