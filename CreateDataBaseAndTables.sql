﻿-- Создание БД и таблиц:
-- 1) Сотрудники
-- 2) Должности
-- 3) Страны
-- 4) Команды
-- 5) Достижения
-- 6) Контракты

GO
CREATE DATABASE FootballClub;

GO
USE FootballClub;

CREATE TABLE Countries ( CountryID INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
CountryName varchar(20), CountryDescription varchar(50));

CREATE TABLE Positions ( PositionID INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
PositionName varchar(30), Salary int, Responsibility varchar(50),
Requirements varchar(40));

CREATE TABLE Contracts( ContractID INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
DateOfSigning Date, ExpirationDate Date, PositionID INT FOREIGN KEY REFERENCES Positions(PositionID) ON DELETE CASCADE ON UPDATE CASCADE);

CREATE TABLE Employees ( EmployeeID INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
LastName varchar(30), Name varchar(30), MiddleName varchar(30), 
Birthday Date, PhoneNumber int,
CountryID INT FOREIGN KEY REFERENCES Countries(CountryID) ON DELETE CASCADE ON UPDATE CASCADE,
ContractID INT FOREIGN KEY REFERENCES Contracts(ContractID) ON DELETE CASCADE ON UPDATE CASCADE);

CREATE TABLE Teams ( TeamID INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
CountryID INT FOREIGN KEY REFERENCES Countries(CountryID) ON DELETE CASCADE ON UPDATE CASCADE,
DateOfEstablishment Date, CoachID INT FOREIGN KEY REFERENCES Employees(EmployeeID));

CREATE TABLE Player ( PlayerID INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
Goals int, Passes int, AmountOfMatches int, Position varchar(30), TeamID INT FOREIGN KEY REFERENCES Teams(TeamID) ON DELETE CASCADE ON UPDATE CASCADE);
