﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using LibDataBase6;
using System.Configuration;

namespace Lab6_App
{
    public partial class DataBaseLab6 : Form
    {
        // Строка соединения с базой данных
        string ConnectionString = ConfigurationManager.ConnectionStrings["FootballClub"].ConnectionString;

        public DataBaseLab6()
        {
            InitializeComponent();
            DisplayCountries("");
            DisplayPositions("");
            DisplayContracts("");
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region WorkWithTableCountry
        // Локальное хранилище
        DataSet DataSetCountry = new DataSet();
        // Адаптер между локальным хранилищем и базой данных
        SqlDataAdapter dataAdapterCountry;

        /// <summary>
        /// загрузка данных в локальное хранилище и отображение их на форме
        /// </summary>
        /// <param name="CountryName"></param>
        private void DisplayCountries(string CountryName)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            LabelInfoCountries.Text = "\r\n Ход выполнения процесса визуализации:\r\n";
            LabelInfoCountries.Refresh();
            try
            {
                conn.Open();
                DataSetCountry.Clear();
                LabelInfoCountries.Text += "1. Соединение с базой данных установлено;\r\n";
                LabelInfoCountries.Refresh();
                SqlCommand MyCommand = new SqlCommand();
                MyCommand.Connection = conn;

                LabelInfoCountries.Text += "2. Отбор данных в локальное хранилище начат;\r\n";
                LabelInfoCountries.Refresh();

                //Команда на выборку с параметром
                MyCommand.CommandText = "SELECT * FROM Countries WHERE CountryName LIKE  '%'+ @CountryName +'%'";
                MyCommand.Parameters.AddWithValue("@CountryName", CountryName);

                dataAdapterCountry = new SqlDataAdapter();
                dataAdapterCountry.SelectCommand = MyCommand;
                dataAdapterCountry.Fill(DataSetCountry, "Countries");

                LabelInfoCountries.Text += "3. отбор данных в локальное хранилище закончен;\r\n";
                LabelInfoCountries.Refresh();

                dataGridViewCountries.DataSource = DataSetCountry.Tables["Countries"].DefaultView;
                dataGridViewCountries.Columns["CountryId"].HeaderText = "Идентификатор страны";
                dataGridViewCountries.Columns["CountryName"].HeaderText = "Название страны";
                dataGridViewCountries.Columns["CountryDescription"].HeaderText = "Описание страны";

                CountryID.Text = DataSetCountry.Tables["Countries"].Rows[0][0].ToString();
                LabelInfoCountries.Text += "4. отображение данных из локального хранилища в табличных элементах управления закончено!!!\r\n";
                LabelInfoCountries.Refresh();
            }
            catch (Exception exeption)
            {
                LabelInfoCountries.Text += "Ошибка: " + exeption.ToString();
                LabelInfoCountries.Refresh();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// При выделения поля DataGrid присваивает элементу Label CountryID значение ID выделенной строки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewCountries_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            CountryID.Text = DataSetCountry.Tables["Countries"].Rows[dataGridViewCountries.SelectedCells[0].RowIndex][0].ToString();
        }

        /// <summary>
        /// Обновляет DataGrid в таблице Countries
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateTableCountries_Click(object sender, EventArgs e)
        {
            DisplayCountries("");
        }

        /// <summary>
        /// Удаляет запись из таблице Countries
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteCountry_Click(object sender, EventArgs e)
        {
            DataBaseConnection.DeleteCountry(ConnectionString, int.Parse(CountryID.Text));
        }

        /// <summary>
        /// Обновляет строку в таблице Countries
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateCountry_Click(object sender, EventArgs e)
        {
            DataBaseConnection.UpdateCountry(ConnectionString, int.Parse(CountryID.Text),
                CountryName.Text != "" ? CountryName.Text : DataSetCountry.Tables["Countries"].Rows[dataGridViewCountries.SelectedCells[0].RowIndex][1].ToString(),
                DiscribeCountry.Text != "" ? DiscribeCountry.Text : DataSetCountry.Tables["Countries"].Rows[dataGridViewCountries.SelectedCells[0].RowIndex][2].ToString());

            DiscribeCountry.Text = CountryName.Text = "";
        }

        /// <summary>
        /// Добавляет запись в таблицу Countries
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddCountry_Click(object sender, EventArgs e)
        {
           if(CountryName.Text != "" && DiscribeCountry.Text != "")
           {
               DataBaseConnection.AddCountry(ConnectionString, CountryName.Text, DiscribeCountry.Text);
               DiscribeCountry.Text = CountryName.Text = "";
           }
        }

        /// <summary>
        /// Обновляет DataGrid с фильтром
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateWithSearchParametrCountry_Click(object sender, EventArgs e)
        {
            DisplayCountries(textBoxSearchCountryTable.Text);
        }
        #endregion
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region WorkWithPositions
        // Локальное хранилище
        DataSet DataSetPosition = new DataSet();
        // Адаптер между локальным хранилищем и базой данных
        SqlDataAdapter dataAdapterPosition;
        /// <summary>
        /// загрузка данных в локальное хранилище и отображение их на форме
        /// </summary>
        /// <param name="PositionName"></param>
        private void DisplayPositions(string PositionName)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            LabelInfoPositions.Text = "\r\n Ход выполнения процесса визуализации:\r\n";
            LabelInfoPositions.Refresh();
            try
            {
                conn.Open();
                DataSetPosition.Clear();
                LabelInfoPositions.Text += "1. Соединение с базой данных установлено;\r\n";
                LabelInfoPositions.Refresh();
                SqlCommand MyCommand = new SqlCommand();
                MyCommand.Connection = conn;

                LabelInfoPositions.Text += "2. Отбор данных в локальное хранилище начат;\r\n";
                LabelInfoPositions.Refresh();

                //Команда на выборку с параметром
                MyCommand.CommandText = "SELECT * FROM Positions WHERE PositionName LIKE  '%'+ @PositionName +'%'";
                MyCommand.Parameters.AddWithValue("@PositionName", PositionName);

                dataAdapterPosition = new SqlDataAdapter();
                dataAdapterPosition.SelectCommand = MyCommand;
                dataAdapterPosition.Fill(DataSetPosition, "Positions");

                LabelInfoPositions.Text += "3. отбор данных в локальное хранилище закончен;\r\n";
                LabelInfoPositions.Refresh();

                dataGridViewPositions.DataSource = DataSetPosition.Tables["Positions"].DefaultView;
                dataGridViewPositions.Columns["PositionID"].HeaderText = "Идентификатор должности";
                dataGridViewPositions.Columns["PositionName"].HeaderText = "Название должности";
                dataGridViewPositions.Columns["Salary"].HeaderText = "Зарплата";
                dataGridViewPositions.Columns["Responsibility"].HeaderText = "Обязанности";
                dataGridViewPositions.Columns["Requirements"].HeaderText = "Требования";

                PositionID.Text = DataSetPosition.Tables["Positions"].Rows[0][0].ToString();
                LabelInfoPositions.Text += "4. отображение данных из локального хранилища в табличных элементах управления закончено!!!\r\n";
                LabelInfoPositions.Refresh();
            }
            catch (Exception exeption)
            {
                LabelInfoPositions.Text += "Ошибка: " + exeption.ToString();
                LabelInfoPositions.Refresh();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Обновление DataGrid с параметром отбора
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchWithSearchParametrPosition_Click(object sender, EventArgs e)
        {
            DisplayPositions(textBoxPositionSearch.Text);
        }

        /// <summary>
        /// По нажатию на строку DataGrid присваивает элементу Label PositionID значения ID на текущей строке таблицы Position
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPositions_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            PositionID.Text = DataSetPosition.Tables["Positions"].Rows[dataGridViewPositions.SelectedCells[0].RowIndex][0].ToString();
        }

        /// <summary>
        /// Заполняет DataGrid всеми значения таблицы Positions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdatePositionTable_Click(object sender, EventArgs e)
        {
            DisplayPositions("");
        }

        /// <summary>
        /// Удаляет запись по из таблицы Position по ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeletePosition_Click(object sender, EventArgs e)
        {
            DataBaseConnection.DeletePosition(ConnectionString, int.Parse(PositionID.Text));
        }

        /// <summary>
        /// Обновляет запись в таблице Position
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdatePosition_Click(object sender, EventArgs e)
        {
            DataBaseConnection.UpdatePosition(ConnectionString, int.Parse(PositionID.Text),
                PositionName.Text != "" ? PositionName.Text : DataSetPosition.Tables["Positions"].Rows[dataGridViewPositions.SelectedCells[0].RowIndex][1].ToString(),
                int.TryParse(PositionSalary.Text, out _) ? int.Parse(PositionSalary.Text) : int.Parse(DataSetPosition.Tables["Positions"].Rows[dataGridViewPositions.SelectedCells[0].RowIndex][2].ToString()),
                PositionResponsibility.Text != "" ? PositionResponsibility.Text : DataSetPosition.Tables["Positions"].Rows[dataGridViewPositions.SelectedCells[0].RowIndex][3].ToString(),
                PositionRequirements.Text != "" ? PositionRequirements.Text : DataSetPosition.Tables["Positions"].Rows[dataGridViewPositions.SelectedCells[0].RowIndex][4].ToString());

            PositionName.Text = PositionSalary.Text = PositionResponsibility.Text = PositionRequirements.Text = "";
        }

        /// <summary>
        /// Добавляет запись в таблицу Position
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddPosition_Click(object sender, EventArgs e)
        {
            if (PositionName.Text != "" && PositionSalary.Text != "" &&
                PositionRequirements.Text != "" && PositionResponsibility.Text != "")
            {
                DataBaseConnection.AddPosition(ConnectionString, PositionName.Text, int.Parse(PositionSalary.Text),
                    PositionResponsibility.Text, PositionRequirements.Text);
            }

            PositionName.Text = PositionSalary.Text = PositionResponsibility.Text = PositionRequirements.Text = "";
        }
        #endregion
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region WorkWithContracts
        // Локальное хранилище
        DataSet DataSetContract = new DataSet();
        // Адаптер между локальным хранилищем и базой данных
        SqlDataAdapter dataAdapterContract;
        private BindingSource bindingSourceContracts;

        /// <summary>
        /// загрузка данных в локальное хранилище и отображение их на форме
        /// </summary>
        /// <param name="DateOfSigning">Дата подписания</param>
        private void DisplayContracts(string DateOfSigning)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            LabelInfoContracts.Text = "\r\n Ход выполнения процесса визуализации:\r\n";
            LabelInfoContracts.Refresh();
            try
            {
                conn.Open();
                dataGridViewContracts.DataSource = null;
                DataSetContract.Clear();
                LabelInfoContracts.Text += "1. cоединение с базой данных установлено;\r\n";
                LabelInfoContracts.Refresh();
                SqlCommand MyCommand = new SqlCommand();
                MyCommand.Connection = conn;

                LabelInfoContracts.Text += "2. отбор данных в локальное хранилище начат;\r\n";
                LabelInfoContracts.Refresh();

                //Команда на выборку   
                MyCommand.CommandText = "SELECT Contracts.ContractID, Contracts.DateOfSigning, Contracts.ExpirationDate, Positions.PositionName, Contracts.PositionID " +
                    "FROM Contracts INNER JOIN Positions ON Contracts.PositionID = Positions.PositionID " +
                    "WHERE DateOfSigning > @DateOfSigning";
                MyCommand.Parameters.AddWithValue("@DateOfSigning", DateOfSigning);

                // Заполнение Data Source данными таблиц Operations, Fuels, Tanks посредством соответствующего метода адаптера
                dataAdapterContract = new SqlDataAdapter();
                dataAdapterContract.SelectCommand = MyCommand;
                dataAdapterContract.Fill(DataSetContract, "Contracts");

                // Вывод сообщений
                LabelInfoContracts.Text += "3. отбор данных в локальное хранилище закончен;\r\n";
                LabelInfoContracts.Refresh();

                // Настройка табличного элемента управления
                bindingSourceContracts = new BindingSource();
                bindingSourceContracts.DataSource = DataSetContract.Tables["Contracts"].DefaultView;
                dataGridViewContracts.DataSource = bindingSourceContracts;
                dataGridViewContracts.Columns["ContractID"].HeaderText = "Индентификатор контракта";
                dataGridViewContracts.Columns["DateOfSigning"].HeaderText = "Дата подписания";
                dataGridViewContracts.Columns["ExpirationDate"].HeaderText = "Дата окончания договора";
                dataGridViewContracts.Columns["PositionID"].Visible = false;
                dataGridViewContracts.Columns["PositionID"].ReadOnly = true;
                dataGridViewContracts.Columns["PositionName"].HeaderText = "Должность";

                // Заполнение списков
                ComboBoxContracts.DataSource = DataSetContract.Tables["Contracts"]; ComboBoxContracts.DisplayMember = "PositionName"; ComboBoxContracts.ValueMember = "PositionID";

                ContractID.Text = DataSetContract.Tables["Contracts"].Rows[0][0].ToString();
                // Вывод сообщений
                LabelInfoContracts.Text += "4. отображение данных из локального хранилища в табличных элементах управления закончено!!!\r\n";
                LabelInfoContracts.Refresh();
            }
            catch (Exception exeption)
            {
                LabelInfoContracts.Text += "Ошибка: " + exeption.ToString();
                LabelInfoContracts.Refresh();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Стандартное событие кнопки, выведет все данные находящиеся в таблице
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateTable_Click(object sender, EventArgs e)
        {
            DisplayContracts("");
        }

        /// <summary>
        /// Обратится к методу, который удалит запись по ID из таблицы Contract
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteContract_Click(object sender, EventArgs e)
        {
            DataBaseConnection.DeleteContract(ConnectionString, int.Parse(ContractID.Text));
        }

        /// <summary>
        /// По нажатию на любую строку элемент Label ContractID получает значение ID из этой строки.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewContracts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ContractID.Text = DataSetContract.Tables["Contracts"].Rows[dataGridViewContracts.SelectedCells[0].RowIndex][0].ToString();
        }

        /// <summary>
        /// Обращается к методу, который обновляет запись по ID  в таблице Contract
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateContract_Click(object sender, EventArgs e)
        {
            if(dateTimePickerSinging.Value < dateTimePickerExpiration.Value)
            {
                DataBaseConnection.UpdateContract(ConnectionString, int.Parse(ContractID.Text),
                dateTimePickerSinging.Value, dateTimePickerExpiration.Value,
                int.Parse(ComboBoxContracts.SelectedValue.ToString()));
            }
        }

        /// <summary>
        /// Обратится к меоду, который добавит новую запись в таблицу Contract
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddContract_Click(object sender, EventArgs e)
        {
            if (dateTimePickerSinging.Value < dateTimePickerExpiration.Value
                && int.TryParse(ComboBoxContracts.SelectedValue.ToString(), out _))
            {
                DataBaseConnection.AddContract(ConnectionString, dateTimePickerSinging.Value, 
                    dateTimePickerExpiration.Value, int.Parse(ComboBoxContracts.SelectedValue.ToString()));
            }
        }

        /// <summary>
        /// Обновляет DataGrid с уловием поиска.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateWithSearchParametrContracts_Click(object sender, EventArgs e)
        {
            DisplayContracts(dateTimePickerSearch.Value.ToString());
        }
        #endregion
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}