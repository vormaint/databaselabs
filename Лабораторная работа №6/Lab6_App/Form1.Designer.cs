﻿namespace Lab6_App
{
    partial class DataBaseLab6
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dateTimePickerSearch = new System.Windows.Forms.DateTimePicker();
            this.UpdateWithSearchParametrContracts = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.UpdateTable = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dateTimePickerExpiration = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerSinging = new System.Windows.Forms.DateTimePicker();
            this.AddContract = new System.Windows.Forms.Button();
            this.UpdateContract = new System.Windows.Forms.Button();
            this.DeleteContract = new System.Windows.Forms.Button();
            this.ComboBoxContracts = new System.Windows.Forms.ComboBox();
            this.ContractID = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.LabelInfoContracts = new System.Windows.Forms.Label();
            this.dataGridViewContracts = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBoxPositionSearch = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.SearchWithSearchParametrPosition = new System.Windows.Forms.Button();
            this.UpdatePositionTable = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PositionSalary = new System.Windows.Forms.TextBox();
            this.PositionRequirements = new System.Windows.Forms.TextBox();
            this.PositionResponsibility = new System.Windows.Forms.TextBox();
            this.PositionName = new System.Windows.Forms.TextBox();
            this.DeletePosition = new System.Windows.Forms.Button();
            this.UpdatePosition = new System.Windows.Forms.Button();
            this.AddPosition = new System.Windows.Forms.Button();
            this.PositionID = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LabelInfoPositions = new System.Windows.Forms.Label();
            this.dataGridViewPositions = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.UpdateWithSearchParametrCountry = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxSearchCountryTable = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CountryID = new System.Windows.Forms.Label();
            this.CountryName = new System.Windows.Forms.TextBox();
            this.DiscribeCountry = new System.Windows.Forms.TextBox();
            this.AddCountry = new System.Windows.Forms.Button();
            this.UpdateCountry = new System.Windows.Forms.Button();
            this.DeleteCountry = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.UpdateTableCountries = new System.Windows.Forms.Button();
            this.LabelInfoCountries = new System.Windows.Forms.Label();
            this.dataGridViewCountries = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewContracts)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPositions)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCountries)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dateTimePickerSearch);
            this.tabPage3.Controls.Add(this.UpdateWithSearchParametrContracts);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.UpdateTable);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.LabelInfoContracts);
            this.tabPage3.Controls.Add(this.dataGridViewContracts);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1142, 521);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Contracts";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dateTimePickerSearch
            // 
            this.dateTimePickerSearch.Location = new System.Drawing.Point(124, 481);
            this.dateTimePickerSearch.Name = "dateTimePickerSearch";
            this.dateTimePickerSearch.Size = new System.Drawing.Size(152, 22);
            this.dateTimePickerSearch.TabIndex = 19;
            // 
            // UpdateWithSearchParametrContracts
            // 
            this.UpdateWithSearchParametrContracts.Location = new System.Drawing.Point(297, 462);
            this.UpdateWithSearchParametrContracts.Name = "UpdateWithSearchParametrContracts";
            this.UpdateWithSearchParametrContracts.Size = new System.Drawing.Size(197, 53);
            this.UpdateWithSearchParametrContracts.TabIndex = 16;
            this.UpdateWithSearchParametrContracts.Text = "Update With Search Parametr";
            this.UpdateWithSearchParametrContracts.UseVisualStyleBackColor = true;
            this.UpdateWithSearchParametrContracts.Click += new System.EventHandler(this.UpdateWithSearchParametrContracts_Click);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 471);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 47);
            this.label4.TabIndex = 14;
            this.label4.Text = "Search DateOfSinging";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UpdateTable
            // 
            this.UpdateTable.Location = new System.Drawing.Point(500, 462);
            this.UpdateTable.Name = "UpdateTable";
            this.UpdateTable.Size = new System.Drawing.Size(236, 53);
            this.UpdateTable.TabIndex = 13;
            this.UpdateTable.Text = "Update Table";
            this.UpdateTable.UseVisualStyleBackColor = true;
            this.UpdateTable.Click += new System.EventHandler(this.UpdateTable_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dateTimePickerExpiration);
            this.groupBox3.Controls.Add(this.dateTimePickerSinging);
            this.groupBox3.Controls.Add(this.AddContract);
            this.groupBox3.Controls.Add(this.UpdateContract);
            this.groupBox3.Controls.Add(this.DeleteContract);
            this.groupBox3.Controls.Add(this.ComboBoxContracts);
            this.groupBox3.Controls.Add(this.ContractID);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(745, 230);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(378, 285);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Работа с данными";
            // 
            // dateTimePickerExpiration
            // 
            this.dateTimePickerExpiration.Location = new System.Drawing.Point(152, 83);
            this.dateTimePickerExpiration.Name = "dateTimePickerExpiration";
            this.dateTimePickerExpiration.Size = new System.Drawing.Size(220, 22);
            this.dateTimePickerExpiration.TabIndex = 18;
            // 
            // dateTimePickerSinging
            // 
            this.dateTimePickerSinging.Location = new System.Drawing.Point(160, 53);
            this.dateTimePickerSinging.Name = "dateTimePickerSinging";
            this.dateTimePickerSinging.Size = new System.Drawing.Size(212, 22);
            this.dateTimePickerSinging.TabIndex = 17;
            // 
            // AddContract
            // 
            this.AddContract.Location = new System.Drawing.Point(28, 232);
            this.AddContract.Name = "AddContract";
            this.AddContract.Size = new System.Drawing.Size(344, 47);
            this.AddContract.TabIndex = 12;
            this.AddContract.Text = "Add Contract";
            this.AddContract.UseVisualStyleBackColor = true;
            this.AddContract.Click += new System.EventHandler(this.AddContract_Click);
            // 
            // UpdateContract
            // 
            this.UpdateContract.Location = new System.Drawing.Point(28, 190);
            this.UpdateContract.Name = "UpdateContract";
            this.UpdateContract.Size = new System.Drawing.Size(344, 36);
            this.UpdateContract.TabIndex = 11;
            this.UpdateContract.Text = "Update Contract";
            this.UpdateContract.UseVisualStyleBackColor = true;
            this.UpdateContract.Click += new System.EventHandler(this.UpdateContract_Click);
            // 
            // DeleteContract
            // 
            this.DeleteContract.Location = new System.Drawing.Point(28, 148);
            this.DeleteContract.Name = "DeleteContract";
            this.DeleteContract.Size = new System.Drawing.Size(344, 36);
            this.DeleteContract.TabIndex = 10;
            this.DeleteContract.Text = "Delete Contract";
            this.DeleteContract.UseVisualStyleBackColor = true;
            this.DeleteContract.Click += new System.EventHandler(this.DeleteContract_Click);
            // 
            // ComboBoxContracts
            // 
            this.ComboBoxContracts.FormattingEnabled = true;
            this.ComboBoxContracts.Location = new System.Drawing.Point(112, 111);
            this.ComboBoxContracts.Name = "ComboBoxContracts";
            this.ComboBoxContracts.Size = new System.Drawing.Size(260, 24);
            this.ComboBoxContracts.TabIndex = 9;
            // 
            // ContractID
            // 
            this.ContractID.AutoSize = true;
            this.ContractID.Location = new System.Drawing.Point(109, 23);
            this.ContractID.Name = "ContractID";
            this.ContractID.Size = new System.Drawing.Size(0, 17);
            this.ContractID.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(21, 113);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 17);
            this.label13.TabIndex = 5;
            this.label13.Text = "Должность :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 83);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(125, 17);
            this.label12.TabIndex = 4;
            this.label12.Text = "Дата окончания :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 17);
            this.label11.TabIndex = 3;
            this.label11.Text = "Дата подписания :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(21, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 17);
            this.label10.TabIndex = 2;
            this.label10.Text = "ContractID :";
            // 
            // LabelInfoContracts
            // 
            this.LabelInfoContracts.Location = new System.Drawing.Point(742, 6);
            this.LabelInfoContracts.Name = "LabelInfoContracts";
            this.LabelInfoContracts.Size = new System.Drawing.Size(392, 221);
            this.LabelInfoContracts.TabIndex = 1;
            // 
            // dataGridViewContracts
            // 
            this.dataGridViewContracts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewContracts.Location = new System.Drawing.Point(6, 6);
            this.dataGridViewContracts.Name = "dataGridViewContracts";
            this.dataGridViewContracts.RowHeadersWidth = 51;
            this.dataGridViewContracts.RowTemplate.Height = 24;
            this.dataGridViewContracts.Size = new System.Drawing.Size(730, 450);
            this.dataGridViewContracts.TabIndex = 0;
            this.dataGridViewContracts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewContracts_CellClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBoxPositionSearch);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.SearchWithSearchParametrPosition);
            this.tabPage2.Controls.Add(this.UpdatePositionTable);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.LabelInfoPositions);
            this.tabPage2.Controls.Add(this.dataGridViewPositions);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1142, 521);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Positions";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBoxPositionSearch
            // 
            this.textBoxPositionSearch.Location = new System.Drawing.Point(106, 476);
            this.textBoxPositionSearch.Name = "textBoxPositionSearch";
            this.textBoxPositionSearch.Size = new System.Drawing.Size(221, 22);
            this.textBoxPositionSearch.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label14.Location = new System.Drawing.Point(9, 467);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 42);
            this.label14.TabIndex = 9;
            this.label14.Text = "Search PositionName";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SearchWithSearchParametrPosition
            // 
            this.SearchWithSearchParametrPosition.Location = new System.Drawing.Point(333, 462);
            this.SearchWithSearchParametrPosition.Name = "SearchWithSearchParametrPosition";
            this.SearchWithSearchParametrPosition.Size = new System.Drawing.Size(173, 53);
            this.SearchWithSearchParametrPosition.TabIndex = 8;
            this.SearchWithSearchParametrPosition.Text = "Search With Search Parametr";
            this.SearchWithSearchParametrPosition.UseVisualStyleBackColor = true;
            this.SearchWithSearchParametrPosition.Click += new System.EventHandler(this.SearchWithSearchParametrPosition_Click);
            // 
            // UpdatePositionTable
            // 
            this.UpdatePositionTable.Location = new System.Drawing.Point(512, 462);
            this.UpdatePositionTable.Name = "UpdatePositionTable";
            this.UpdatePositionTable.Size = new System.Drawing.Size(224, 53);
            this.UpdatePositionTable.TabIndex = 7;
            this.UpdatePositionTable.Text = "Update Table";
            this.UpdatePositionTable.UseVisualStyleBackColor = true;
            this.UpdatePositionTable.Click += new System.EventHandler(this.UpdatePositionTable_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PositionSalary);
            this.groupBox2.Controls.Add(this.PositionRequirements);
            this.groupBox2.Controls.Add(this.PositionResponsibility);
            this.groupBox2.Controls.Add(this.PositionName);
            this.groupBox2.Controls.Add(this.DeletePosition);
            this.groupBox2.Controls.Add(this.UpdatePosition);
            this.groupBox2.Controls.Add(this.AddPosition);
            this.groupBox2.Controls.Add(this.PositionID);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(745, 219);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(377, 283);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Работа с данными";
            // 
            // PositionSalary
            // 
            this.PositionSalary.Location = new System.Drawing.Point(103, 89);
            this.PositionSalary.Name = "PositionSalary";
            this.PositionSalary.Size = new System.Drawing.Size(268, 22);
            this.PositionSalary.TabIndex = 14;
            // 
            // PositionRequirements
            // 
            this.PositionRequirements.Location = new System.Drawing.Point(119, 143);
            this.PositionRequirements.Name = "PositionRequirements";
            this.PositionRequirements.Size = new System.Drawing.Size(252, 22);
            this.PositionRequirements.TabIndex = 13;
            // 
            // PositionResponsibility
            // 
            this.PositionResponsibility.Location = new System.Drawing.Point(127, 118);
            this.PositionResponsibility.Name = "PositionResponsibility";
            this.PositionResponsibility.Size = new System.Drawing.Size(244, 22);
            this.PositionResponsibility.TabIndex = 12;
            // 
            // PositionName
            // 
            this.PositionName.Location = new System.Drawing.Point(178, 63);
            this.PositionName.Name = "PositionName";
            this.PositionName.Size = new System.Drawing.Size(193, 22);
            this.PositionName.TabIndex = 11;
            // 
            // DeletePosition
            // 
            this.DeletePosition.Location = new System.Drawing.Point(20, 175);
            this.DeletePosition.Name = "DeletePosition";
            this.DeletePosition.Size = new System.Drawing.Size(351, 33);
            this.DeletePosition.TabIndex = 8;
            this.DeletePosition.Text = "Delete Position";
            this.DeletePosition.UseVisualStyleBackColor = true;
            this.DeletePosition.Click += new System.EventHandler(this.DeletePosition_Click);
            // 
            // UpdatePosition
            // 
            this.UpdatePosition.Location = new System.Drawing.Point(20, 214);
            this.UpdatePosition.Name = "UpdatePosition";
            this.UpdatePosition.Size = new System.Drawing.Size(351, 34);
            this.UpdatePosition.TabIndex = 9;
            this.UpdatePosition.Text = "Update Position";
            this.UpdatePosition.UseVisualStyleBackColor = true;
            this.UpdatePosition.Click += new System.EventHandler(this.UpdatePosition_Click);
            // 
            // AddPosition
            // 
            this.AddPosition.Location = new System.Drawing.Point(20, 254);
            this.AddPosition.Name = "AddPosition";
            this.AddPosition.Size = new System.Drawing.Size(351, 29);
            this.AddPosition.TabIndex = 10;
            this.AddPosition.Text = "Add Position";
            this.AddPosition.UseVisualStyleBackColor = true;
            this.AddPosition.Click += new System.EventHandler(this.AddPosition_Click);
            // 
            // PositionID
            // 
            this.PositionID.AutoSize = true;
            this.PositionID.Location = new System.Drawing.Point(102, 33);
            this.PositionID.Name = "PositionID";
            this.PositionID.Size = new System.Drawing.Size(0, 17);
            this.PositionID.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 146);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 17);
            this.label9.TabIndex = 6;
            this.label9.Text = "Требования :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 17);
            this.label8.TabIndex = 5;
            this.label8.Text = "Обязанности :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "Зарплата :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(155, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Название должности :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "PositionID :";
            // 
            // LabelInfoPositions
            // 
            this.LabelInfoPositions.Location = new System.Drawing.Point(742, 6);
            this.LabelInfoPositions.Name = "LabelInfoPositions";
            this.LabelInfoPositions.Size = new System.Drawing.Size(392, 210);
            this.LabelInfoPositions.TabIndex = 1;
            // 
            // dataGridViewPositions
            // 
            this.dataGridViewPositions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPositions.Location = new System.Drawing.Point(6, 6);
            this.dataGridViewPositions.Name = "dataGridViewPositions";
            this.dataGridViewPositions.RowHeadersWidth = 51;
            this.dataGridViewPositions.RowTemplate.Height = 24;
            this.dataGridViewPositions.Size = new System.Drawing.Size(730, 450);
            this.dataGridViewPositions.TabIndex = 0;
            this.dataGridViewPositions.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPositions_CellClick);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.UpdateWithSearchParametrCountry);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.textBoxSearchCountryTable);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.UpdateTableCountries);
            this.tabPage1.Controls.Add(this.LabelInfoCountries);
            this.tabPage1.Controls.Add(this.dataGridViewCountries);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1142, 521);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "CountryTable";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // UpdateWithSearchParametrCountry
            // 
            this.UpdateWithSearchParametrCountry.Location = new System.Drawing.Point(331, 462);
            this.UpdateWithSearchParametrCountry.Name = "UpdateWithSearchParametrCountry";
            this.UpdateWithSearchParametrCountry.Size = new System.Drawing.Size(160, 56);
            this.UpdateWithSearchParametrCountry.TabIndex = 6;
            this.UpdateWithSearchParametrCountry.Text = "Update With Search Parametr";
            this.UpdateWithSearchParametrCountry.UseVisualStyleBackColor = true;
            this.UpdateWithSearchParametrCountry.Click += new System.EventHandler(this.UpdateWithSearchParametrCountry_Click);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(6, 469);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 43);
            this.label15.TabIndex = 5;
            this.label15.Text = "Search CountryName";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxSearchCountryTable
            // 
            this.textBoxSearchCountryTable.Location = new System.Drawing.Point(113, 484);
            this.textBoxSearchCountryTable.Name = "textBoxSearchCountryTable";
            this.textBoxSearchCountryTable.Size = new System.Drawing.Size(212, 22);
            this.textBoxSearchCountryTable.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CountryID);
            this.groupBox1.Controls.Add(this.CountryName);
            this.groupBox1.Controls.Add(this.DiscribeCountry);
            this.groupBox1.Controls.Add(this.AddCountry);
            this.groupBox1.Controls.Add(this.UpdateCountry);
            this.groupBox1.Controls.Add(this.DeleteCountry);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(755, 237);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(379, 287);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Работа с данными";
            // 
            // CountryID
            // 
            this.CountryID.AutoSize = true;
            this.CountryID.Location = new System.Drawing.Point(90, 34);
            this.CountryID.Name = "CountryID";
            this.CountryID.Size = new System.Drawing.Size(0, 17);
            this.CountryID.TabIndex = 8;
            // 
            // CountryName
            // 
            this.CountryName.Location = new System.Drawing.Point(140, 78);
            this.CountryName.Name = "CountryName";
            this.CountryName.Size = new System.Drawing.Size(233, 22);
            this.CountryName.TabIndex = 7;
            // 
            // DiscribeCountry
            // 
            this.DiscribeCountry.Location = new System.Drawing.Point(153, 127);
            this.DiscribeCountry.Name = "DiscribeCountry";
            this.DiscribeCountry.Size = new System.Drawing.Size(220, 22);
            this.DiscribeCountry.TabIndex = 6;
            // 
            // AddCountry
            // 
            this.AddCountry.Location = new System.Drawing.Point(9, 250);
            this.AddCountry.Name = "AddCountry";
            this.AddCountry.Size = new System.Drawing.Size(364, 37);
            this.AddCountry.TabIndex = 5;
            this.AddCountry.Text = "Add Country";
            this.AddCountry.UseVisualStyleBackColor = true;
            this.AddCountry.Click += new System.EventHandler(this.AddCountry_Click);
            // 
            // UpdateCountry
            // 
            this.UpdateCountry.Location = new System.Drawing.Point(9, 209);
            this.UpdateCountry.Name = "UpdateCountry";
            this.UpdateCountry.Size = new System.Drawing.Size(364, 35);
            this.UpdateCountry.TabIndex = 4;
            this.UpdateCountry.Text = "UpdateCountry";
            this.UpdateCountry.UseVisualStyleBackColor = true;
            this.UpdateCountry.Click += new System.EventHandler(this.UpdateCountry_Click);
            // 
            // DeleteCountry
            // 
            this.DeleteCountry.Location = new System.Drawing.Point(9, 167);
            this.DeleteCountry.Name = "DeleteCountry";
            this.DeleteCountry.Size = new System.Drawing.Size(364, 36);
            this.DeleteCountry.TabIndex = 3;
            this.DeleteCountry.Text = "Delete Country";
            this.DeleteCountry.UseVisualStyleBackColor = true;
            this.DeleteCountry.Click += new System.EventHandler(this.DeleteCountry_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Описание страны:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Название страны:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "CountryID:";
            // 
            // UpdateTableCountries
            // 
            this.UpdateTableCountries.Location = new System.Drawing.Point(497, 460);
            this.UpdateTableCountries.Name = "UpdateTableCountries";
            this.UpdateTableCountries.Size = new System.Drawing.Size(239, 58);
            this.UpdateTableCountries.TabIndex = 2;
            this.UpdateTableCountries.Text = "Update Table";
            this.UpdateTableCountries.UseVisualStyleBackColor = true;
            this.UpdateTableCountries.Click += new System.EventHandler(this.UpdateTableCountries_Click);
            // 
            // LabelInfoCountries
            // 
            this.LabelInfoCountries.Location = new System.Drawing.Point(752, 6);
            this.LabelInfoCountries.Name = "LabelInfoCountries";
            this.LabelInfoCountries.Size = new System.Drawing.Size(382, 228);
            this.LabelInfoCountries.TabIndex = 1;
            // 
            // dataGridViewCountries
            // 
            this.dataGridViewCountries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCountries.Location = new System.Drawing.Point(6, 6);
            this.dataGridViewCountries.Name = "dataGridViewCountries";
            this.dataGridViewCountries.RowHeadersWidth = 51;
            this.dataGridViewCountries.RowTemplate.Height = 24;
            this.dataGridViewCountries.Size = new System.Drawing.Size(730, 450);
            this.dataGridViewCountries.TabIndex = 0;
            this.dataGridViewCountries.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCountries_CellClick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(-1, -2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1150, 550);
            this.tabControl1.TabIndex = 0;
            // 
            // DataBaseLab6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1149, 568);
            this.Controls.Add(this.tabControl1);
            this.Name = "DataBaseLab6";
            this.Text = "DataBaseLab6";
            this.tabPage3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewContracts)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPositions)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCountries)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button UpdatePositionTable;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox PositionSalary;
        private System.Windows.Forms.TextBox PositionRequirements;
        private System.Windows.Forms.TextBox PositionResponsibility;
        private System.Windows.Forms.TextBox PositionName;
        private System.Windows.Forms.Button DeletePosition;
        private System.Windows.Forms.Button UpdatePosition;
        private System.Windows.Forms.Button AddPosition;
        private System.Windows.Forms.Label PositionID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LabelInfoPositions;
        private System.Windows.Forms.DataGridView dataGridViewPositions;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label CountryID;
        private System.Windows.Forms.TextBox CountryName;
        private System.Windows.Forms.TextBox DiscribeCountry;
        private System.Windows.Forms.Button AddCountry;
        private System.Windows.Forms.Button UpdateCountry;
        private System.Windows.Forms.Button DeleteCountry;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button UpdateTableCountries;
        private System.Windows.Forms.Label LabelInfoCountries;
        private System.Windows.Forms.DataGridView dataGridViewCountries;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Label ContractID;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label LabelInfoContracts;
        private System.Windows.Forms.DataGridView dataGridViewContracts;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button UpdateTable;
        private System.Windows.Forms.Button AddContract;
        private System.Windows.Forms.Button UpdateContract;
        private System.Windows.Forms.Button DeleteContract;
        private System.Windows.Forms.ComboBox ComboBoxContracts;
        private System.Windows.Forms.Button UpdateWithSearchParametrContracts;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPositionSearch;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button SearchWithSearchParametrPosition;
        private System.Windows.Forms.TextBox textBoxSearchCountryTable;
        private System.Windows.Forms.Button UpdateWithSearchParametrCountry;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker dateTimePickerSearch;
        private System.Windows.Forms.DateTimePicker dateTimePickerExpiration;
        private System.Windows.Forms.DateTimePicker dateTimePickerSinging;
    }
}

