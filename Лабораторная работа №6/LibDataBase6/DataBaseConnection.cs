﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace LibDataBase6
{
    public static class DataBaseConnection
    {
        /// <summary>
        /// Удаляет запись из таблицы Countries
        /// </summary>
        /// <param name="ConnectionString">Строка соединения</param>
        /// <param name="CountryID">ID страны</param>
        public static void DeleteCountry(string ConnectionString, int CountryID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlParameter ID = new SqlParameter("@ID", CountryID.ToString());
                string strSql = "DELETE FROM Countries WHERE CountryID = @ID";
                SqlCommand command = new SqlCommand(strSql, connection);
                command.Parameters.Add(ID);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Добавляет страну в таблицу Countries
        /// </summary>
        /// <param name="ConnectionString">Строка соединения</param>
        /// <param name="CountryName">Название страны</param>
        /// <param name="CountryDiscribe">Описание страны</param>
        public static void AddCountry(string ConnectionString, string CountryName,
            string CountryDiscribe)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlParameter Name = new SqlParameter("@Name", CountryName.ToString());
                SqlParameter Discribe = new SqlParameter("@Discribe", CountryDiscribe.ToString());
                string strSql = "INSERT INTO Countries(CountryName, CountryDescription) VALUES (@Name, @Discribe)";
                SqlCommand command = new SqlCommand(strSql, connection);
                command.Parameters.Add(Name);
                command.Parameters.Add(Discribe);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Обновляет запись в таблице Countries
        /// </summary>
        /// <param name="ConnectionString">Строка соединения</param>
        /// <param name="CountryID">ID страны</param>
        /// <param name="CountryName">Название страны</param>
        /// <param name="CountryDiscribe">Описание страны</param>
        public static void UpdateCountry(string ConnectionString, int CountryID,
            string CountryName, string CountryDiscribe)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlParameter ID = new SqlParameter("@ID", CountryID.ToString());
                SqlParameter Name = new SqlParameter("@Name", CountryName.ToString());
                SqlParameter Discribe = new SqlParameter("@Discribe", CountryDiscribe.ToString());
                string strSql = "UPDATE Countries " +
                    "SET CountryName = @Name, CountryDescription = @Discribe " +
                    "WHERE CountryID = @ID";
                SqlCommand command = new SqlCommand(strSql, connection);
                command.Parameters.Add(Name);
                command.Parameters.Add(ID);
                command.Parameters.Add(Discribe);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Удаляет запись из таблицы Position
        /// </summary>
        /// <param name="ConnectionString">Строка соединения</param>
        /// <param name="positionID">ID должности</param>
        public static void DeletePosition(string ConnectionString, int positionID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlParameter ID = new SqlParameter("@ID", positionID.ToString());
                string strSql = "DELETE FROM Positions WHERE PositionID = @ID";
                SqlCommand command = new SqlCommand(strSql, connection);
                command.Parameters.Add(ID);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Добавление должности в таблицу Positions
        /// </summary>
        /// <param name="ConnectionString">Строка соединения</param>
        /// <param name="PositionName">Название должности</param>
        /// <param name="Salary"></param>
        /// <param name="Responsibility"></param>
        /// <param name="Requirements"></param>
        public static void AddPosition(string ConnectionString, string PositionName,
            int Salary, string Responsibility, string Requirements)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlParameter Name = new SqlParameter("@Name", PositionName.ToString());
                SqlParameter salary = new SqlParameter("@Salary", Salary.ToString());
                SqlParameter responsibility = new SqlParameter("@Responsibility", Responsibility.ToString());
                SqlParameter requirements = new SqlParameter("@Requirements", Requirements.ToString());
                string strSql = "INSERT INTO Positions VALUES (@Name, @Salary, @Responsibility, @Requirements)";
                SqlCommand command = new SqlCommand(strSql, connection);
                command.Parameters.Add(Name);
                command.Parameters.Add(salary);
                command.Parameters.Add(responsibility);
                command.Parameters.Add(requirements);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Обновление записи в таблице Positions
        /// </summary>
        /// <param name="ConnectionString">Строка соединения</param>
        /// <param name="PositionID">ID должности</param>
        /// <param name="PositionName">Название должности</param>
        /// <param name="Salary">Зарплата</param>
        /// <param name="Responsibility">Обязанности</param>
        /// <param name="Requirements">Требования</param>
        public static void UpdatePosition(string ConnectionString, int PositionID, string PositionName,
            int Salary, string Responsibility, string Requirements)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlParameter Name = new SqlParameter("@Name", PositionName.ToString());
                SqlParameter salary = new SqlParameter("@Salary", Salary.ToString());
                SqlParameter responsibility = new SqlParameter("@Responsibility", Responsibility.ToString());
                SqlParameter requirements = new SqlParameter("@Requirements", Requirements.ToString());
                SqlParameter ID = new SqlParameter("@ID", PositionID.ToString());
                string strSql = "UPDATE Positions " +
                    "SET PositionName = @Name, Salary = @Salary, Responsibility = @Responsibility, Requirements = @Requirements " +
                    "WHERE PositionID = @ID";
                SqlCommand command = new SqlCommand(strSql, connection);
                command.Parameters.Add(Name);
                command.Parameters.Add(salary);
                command.Parameters.Add(responsibility);
                command.Parameters.Add(ID);
                command.Parameters.Add(requirements);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Удаляет запись о контракте из таблицы Contract
        /// </summary>
        /// <param name="ConnectionString">Строка соединения</param>
        /// <param name="ContractID">ID контракта</param>
        public static void DeleteContract(string ConnectionString, int ContractID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlParameter ID = new SqlParameter("@ID", ContractID.ToString());
                string strSql = "DELETE FROM Contracts WHERE ContractID = @ID";
                SqlCommand command = new SqlCommand(strSql, connection);
                command.Parameters.Add(ID);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Обновляет запись о контрактах в таблице Contracts
        /// </summary>
        /// <param name="ConnectionString">строка соединения</param>
        /// <param name="ContractID">ID контракта</param>
        /// <param name="DateOfSinging">Дата подписания</param>
        /// <param name="ExpirationDate">Дата окончания</param>
        /// <param name="PositionID">ID должности</param>
        public static void UpdateContract(string ConnectionString,int ContractID, DateTime DateOfSinging, 
            DateTime ExpirationDate, int PositionID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlParameter ID = new SqlParameter("@PositionID", PositionID.ToString());
                SqlParameter contractID = new SqlParameter("@ContractID", ContractID.ToString());
                SqlParameter dateOfSinging = new SqlParameter("@DateOfSinging", DateOfSinging.ToString());
                SqlParameter expirationDate = new SqlParameter("@ExpirationDate", ExpirationDate.ToString());
                string strSql = "UPDATE Contracts " +
                    "SET DateOfSigning = @DateOfSinging, ExpirationDate = @ExpirationDate, PositionID = @PositionID " +
                    "WHERE ContractID = @ContractID";
                SqlCommand command = new SqlCommand(strSql, connection);
                command.Parameters.Add(ID);
                command.Parameters.Add(contractID);
                command.Parameters.Add(dateOfSinging);
                command.Parameters.Add(expirationDate);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Добавление новой записи в таблицу Contracts
        /// </summary>
        /// <param name="ConnectionString">Строка соединения</param>
        /// <param name="DateOfSinging">Дата подписания</param>
        /// <param name="ExpirationDate">Дата окончания</param>
        /// <param name="PositionID">ID должности</param>
        public static void AddContract(string ConnectionString, DateTime DateOfSinging,
            DateTime ExpirationDate, int PositionID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlParameter ID = new SqlParameter("@PositionID", PositionID.ToString());
                SqlParameter dateOfSinging = new SqlParameter("@DateOfSinging", DateOfSinging.ToString());
                SqlParameter expirationDate = new SqlParameter("@ExpirationDate", ExpirationDate.ToString());
                string strSql = "INSERT INTO Contracts VALUES (@DateOfSinging, @ExpirationDate, @PositionID)";
                SqlCommand command = new SqlCommand(strSql, connection);
                command.Parameters.Add(ID);
                command.Parameters.Add(dateOfSinging);
                command.Parameters.Add(expirationDate);
                command.ExecuteNonQuery();
            }
        }
    }
}
